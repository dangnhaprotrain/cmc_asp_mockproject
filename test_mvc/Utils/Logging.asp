﻿<%
'Logging class
Class Logging
    
    Dim logFile

    private sub Class_Initialize()
        logFile = "d:\log\test.txt"
    end sub

    public function LogError(message)
        Dim filesys, filetxt
        Const ForReading = 1, ForWriting = 2, ForAppending = 8
        Set filesys = CreateObject("Scripting.FileSystemObject")
        Set filetxt = filesys.OpenTextFile(logFile, ForAppending, True)
        filetxt.WriteLine(Now & ": "  & message)
        filetxt.Close 
    end function
End Class
%>