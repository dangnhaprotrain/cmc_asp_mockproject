<%
Response.LCID = 1043 ' Brazilian LCID (use your locale code here).
' Cold also be the LCID property of the page declaration or Session.LCID to set it to the entire session.
%>
<%
	Class ConfigUtils
        public function loadConfig()
        
            dim jsonObj, jsonString, jsonArr, outputObj

            dim fs, f
            Set fs=Server.CreateObject("Scripting.FileSystemObject")
            Set f=fs.OpenTextFile(Server.MapPath("/Utils/systems.config"), 1)

            dim jsonConfig
            do while f.AtEndOfStream = false
                jsonConfig = jsonConfig & f.ReadLine
            loop

            f.Close
            Set f=Nothing
            Set fs=Nothing

            set jsonObj = new JSONobject
            set jsonArr = new jsonArray

            set outputObj = jsonObj.parse(jsonConfig)
           
            set loadConfig = jsonObj
            
        end function

        public function buildConnectionString()
            dim strSecretKey
            dim strDriver, strServer, strUid, strPwd, strDatabase
            dim decryptUID, decryptPWD

            dim jsonObj
            Set jsonObj = loadConfig()
            
            strSecretKey = jsonObj.value("strSecretKey")
            strDriver = jsonObj.value("strDriver")
            strServer = jsonObj.value("strServer")
            strUid = jsonObj.value("strUid")
            strPwd = jsonObj.value("strPwd")
            strDatabase = jsonObj.value("strDatabase")

            decryptUID = RC4DeCryptASC(strUid, strSecretKey)
            decryptPWD = RC4DeCryptASC(strPwd, strSecretKey)

            buildConnectionString = "DRIVER="& strDriver &";SERVER="& strServer &";UID="& decryptUID &";PWD="& decryptPWD &";DATABASE="& strDatabase &""
        end function

	End Class
	
	dim config : set config = new ConfigUtils
	 
%>