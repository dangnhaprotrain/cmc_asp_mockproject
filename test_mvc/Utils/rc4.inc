﻿<%
Dim sbox(255)
   Dim rc4Key(255)

   Sub RC4Initialize(strPwd)

      dim tempSwap, a, b, intLength      

      intLength = len(strPwd)

      for a = 0 To 255

         rc4Key(a)  = ascW(mid(strpwd, (a mod intLength)+1, 1))

         sbox(a) = a                       
      next

      b = 0

      for a = 0 To 255
         b = (b + sbox(a) + rc4Key(a)) Mod 256         
         tempSwap = sbox(a)
         sbox(a) = sbox(b)
         sbox(b) = tempSwap
      Next

   End Sub

   function EnDeCrypt(plaintxt, psw)      

      dim temp, a, i, j, k, cipherby, cipher      

      i = 0
      j = 0

      RC4Initialize psw

      for a = 1 To Len(plaintxt)
         i  = (i + 1) Mod 256
         j  = (j + sbox(i)) Mod 256
         temp   = sbox(i)
         sbox(i)= sbox(j)
         sbox(j)= temp

         k  = sbox((sbox(i) + sbox(j)) Mod 256)

         cipherby   = ascW(Mid(plaintxt, a, 1)) Xor k
         cipher     = cipher & chrW(cipherby)         
      next

      enDeCrypt = cipher            

   end function

   function RC4EnCryptASC(plaintxt, psw)      

      dim temp, a, i, j, k, cipherby, cipher      

      i = 0
      j = 0

      RC4Initialize psw

      for a = 1 To Len(plaintxt)
         i  = (i + 1) Mod 256
         j  = (j + sbox(i)) Mod 256
         temp   = sbox(i)
         sbox(i)= sbox(j)
         sbox(j)= temp

         k      = sbox((sbox(i) + sbox(j)) Mod 256)

         cipherby   = ascW(Mid(plaintxt, a, 1)) Xor k         
         cipher     = cipher & "|"& cipherby         
      next            

      RC4EnCryptASC = cipher            

   end function

   function RC4DeCryptASC(plaintxt, psw)      
      dim plaintxtCHR
      plaintxtCHR = transformToCHR(plaintxt)

      dim temp, a, i, j, k, cipherby, cipher      

      i = 0
      j = 0      

      dim arrayEncrypted            

      RC4Initialize psw         

       for a = 1 To Len(plaintxtCHR)
         i  = (i + 1) Mod 256
         j  = (j + sbox(i)) Mod 256
         temp   = sbox(i)
         sbox(i)= sbox(j)
         sbox(j)= temp

         k  = sbox((sbox(i) + sbox(j)) Mod 256)

         cipherby   = ascW(Mid(plaintxtCHR, a, 1)) Xor k
         cipher     = cipher & chrW(cipherby)         
      next

      RC4DeCryptASC = cipher                       

   end function

function transformToCHR(plaintxt)


      dim returnText, arrayEncrypted
      arrayEncrypted = split(plaintxt, "|")            

      returnText=""            

      for a = 1 to ubound(arrayEncrypted)         
         returnText=returnText&chrW(arrayEncrypted(a))         
      next            

      transformToCHR = returnText
end function
%>