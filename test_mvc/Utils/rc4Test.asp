﻿<%@ Language="VBSCRIPT" CODEPAGE="65001"%>
<!DOCTYPE html>
<html lang="en">
  <head runat="server">
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
      </head>
<!-- #include file="rc4.inc" -->
<%
    dim action
    action = Request.form("action")

    dim plainTextE, secretKeyE, encryptTextE
    secretKeyE = Request.form("secretKeyE")
    plainTextE = Request.form("plainTextE")

    if action = "Encrypt" then
         encryptTextE = RC4EnCryptASC(plainTextE, secretKeyE)
    end if

    dim plainTextD, secretKeyD, encryptTextD
    encryptTextD = Request.form("encryptTextD")
    secretKeyD = Request.form("secretKeyD")
    if action = "Decrypt" then
         plainTextD = RC4DeCryptASC(encryptTextD, secretKeyD)
    end if
 
%>
<body>
    <form method="post">
        <input type="hidden" name="action" value="Encrypt">
        <h1> Encrypt </h1>
        Plain Text: <input type="text" name="plainTextE" value="<%=plainTextE%>" />
        <br>
        <br>
        Secret Key: <input type="text" name="secretKeyE" value="<%=secretKeyE%>" />
        <br>
        <br>
        <input  type="submit" value="Encrypt"></input>
        <br>
        Encrypt text:<%=encryptTextE%>
    </form>


    <form method="post">
        <input type="hidden" name="action" value="Decrypt">
        <h1> Decrypt </h1>
        Encrypt text: <input type="text" name="encryptTextD" value="<%=encryptTextD%>" />
        <br>
        <br>
        Secret Key: <input type="text" name="secretKeyD" value="<%=secretKeyD%>" />
        <br>
        <br>
        <input  type="submit" value="Decrypt"></input>
        <br>
        Plain Text:<%=plainTextD%>
    </form>
</body>
</html>