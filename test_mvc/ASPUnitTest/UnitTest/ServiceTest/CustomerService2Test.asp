﻿
<%
Class CustomerService2Test

	Public Function TestCaseNames()
		TestCaseNames = Array("FindByIdTest1", "CreateTest1")
	End Function

	Public Sub SetUp()
		'Response.Write("SetUp<br>")
	End Sub

	Public Sub TearDown()
		'Response.Write("TearDown<br>")
	End Sub

    Public Sub FindByIdTest1(oTestResult)
        Dim CustomerSrvc : Set CustomerSrvc = (new CustomerService)()
        Call CustomerSrvc.Find("4")
        Call oTestResult.AssertEquals("a2222aaa", CustomerSrvc.Name, "a2222aaa = a2222aaa, Should not fail!")
    End Sub

    Public Sub CreateTest1(oTestResult)
        Dim CustomerSrvc : Set CustomerSrvc = (new CustomerService)()
        With CustomerSrvc
            .name = "Name"
            .phone = "0976765678"
            .description = "Description"
            '.updateDatetime = Now
            .status = 1
            '.createDatetime = Now
            .Save()
        End With
    End Sub

End Class
%>